import SwiftUI
import Combine


struct ContentView: View {
    @EnvironmentObject var settings: Settings
    @EnvironmentObject var keyboard: Keyboard

    var body: some View {
        bodyView()
    }
    func bodyView() -> AnyView {
        switch self.settings.screen {
        case .loginPhone:
            return AnyView(LoginPhoneScreen().environmentObject(settings).environmentObject(keyboard))
        case .loginPincode:
            return AnyView(LoginPincodeScreen().environmentObject(settings))
        case .loginSMS:
            return AnyView(LoginSMSScreen().environmentObject(settings).environmentObject(keyboard))
        case .main:
            return AnyView(MainScreen().environmentObject(settings))
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static let keyboard = Keyboard()
    static let settings = Settings()
    static var previews: some View {
        ContentView().environmentObject(settings).environmentObject(keyboard)
    }
}
