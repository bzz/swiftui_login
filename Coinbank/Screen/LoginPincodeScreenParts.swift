import SwiftUI


struct PinButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(width: 73.0, height: 73.0)
            .font(.custom("Montserrat-Regular", size: 30))
            .background(configuration.isPressed ? Color.customYellow : Color.white)
            .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
            .cornerRadius(73.0)
            .padding(15)
    }
}

struct PinButton: View {
    @EnvironmentObject var settings: Settings
    @ObservedObject var model: LoginPincodeModel
    @State var value: Int

    @State var isPressed = false

    var body: some View {
        Button(action: {
            switch self.model.pin_status {
            case .pin_enter:
                self.model.pin.removeFirst()
                self.model.pin.append(self.value)
                if !self.model.pin.contains(-1) {
                    self.model.pin_status = .pin_repeat
                    self.model.pin_repeat = self.model.pin
                    self.model.pin = [-1, -1, -1, -1]
                }
            case .pin_repeat:
                self.model.pin.removeFirst()
                self.model.pin.append(self.value)
                if !self.model.pin.contains(-1) {
                    if self.model.pin_repeat == self.model.pin {
                        self.settings.screen = .loginSMS
                    } else {
                        self.model.pin_status = .pin_repeat_wrong
                        self.model.pin = [-1, -1, -1, -1]
                    }
                }
            case .pin_repeat_wrong:
                self.model.pin.removeFirst()
                self.model.pin.append(self.value)
                if !self.model.pin.contains(-1) {
                    self.model.pin_status = .pin_repeat
                    self.model.pin_repeat = self.model.pin
                    self.model.pin = [-1, -1, -1, -1]
                }
            }

        }) {
            Text(String(value))
        }
            .buttonStyle(PinButtonStyle())
    }
}

struct PincodePad: View {
    @ObservedObject var model: LoginPincodeModel

    var body: some View {
        VStack {
            Text("Enter 4-digit PIN-code")
                .font(.custom("Montserrat-Regular", size: 13))
                .multilineTextAlignment(.center)
                .padding(.bottom, 20.0)
                .foregroundColor(Color.customGray)
            HStack {
                if model.pin_status == .pin_repeat_wrong && model.pin == [-1, -1, -1, -1] || model.pin.count > 4 {
                    ForEach(0 ..< 4) { _ in
                        Circle().fill(Color.red)
                    }
                } else {
                    ForEach(0 ..< 4) { i in
                        if self.model.pin[3 - i] < 0 {
                            Circle().fill(Color.customGray)
                        } else {
                            Circle().fill(Color.customYellow)
                        }
                    }
                }
            }
                .frame(width: 120, height: 8)
                .foregroundColor(Color.customGray)
            Spacer().frame(height: 40)
            ForEach(0 ..< 3) { i in
                HStack {
                    ForEach(1 ..< 4) { ii in
                        PinButton(model: self.model, value: ii + (i * 3))
                    }
                }
            }
            HStack {
                Rectangle().frame(width: 73.0, height: 73.0).foregroundColor(.clear)
                PinButton(model: self.model, value: 0)
                Button(action: {
                    if self.model.pin.count > 0 {
                        self.model.pin.removeLast()
                        self.model.pin.insert(-1, at: 0)
                    }
                }) {
                    Image.init(systemName: "delete.left.fill")
                        .imageScale(.medium)
                        .foregroundColor(.customGray)

                }
                    .frame(width: 73.0, height: 73.0)
                    .font(.custom("Montserrat-Regular", size: 30))
            }
        }
            .foregroundColor(Color.black)
            .multilineTextAlignment(.center)
    }
}
