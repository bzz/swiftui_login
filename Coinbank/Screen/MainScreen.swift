import SwiftUI


struct MainScreen: View {
    @EnvironmentObject var settings: Settings
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MainScreen_Previews: PreviewProvider {
    static let settings = Settings()
    static var previews: some View {
        MainScreen().environmentObject(settings)
    }
}
