import Foundation
import libPhoneNumberiOS


class LoginPhoneModel: ObservableObject {
    @Published var phone: String = ""
    
    func isPhoneValid(_ phone: String, countryNameCode: String) -> Bool {
        let util = NBPhoneNumberUtil()
        do {
            let n = try util.parse(phone, defaultRegion: countryNameCode)
            if util.isValidNumber(n) {
                return true
            }
        }
        catch { }
        return false
    }

    func isWrongPhoneNumber(_ phone: String, countryNameCode: String) -> Bool {
        if !phone.isEmpty && !isPhoneValid(phone, countryNameCode: countryNameCode) {
            return true
        }
        return false
    }

    func getPhone(_ phone: String, countryNameCode: String) -> String {
        var out = ""
        let util = NBPhoneNumberUtil()
        do {
            let n = try util.parse(phone, defaultRegion: countryNameCode)
            out = try util.format(n, numberFormat: .E164)
        }
        catch { }
        return out
    }

    func validatePhoneString(_ phoneString: String, countryNameCode: String) -> String {
        return NBAsYouTypeFormatter(regionCode: countryNameCode)!.inputString(phoneString)
    }

}

