import SwiftUI
import Combine


struct LoginPhoneScreen: View {
//    init() {
//        postman.checkPhoneNumber("222", completion: { phoneAvailable in
//            print(phoneAvailable) })
//    }


    @EnvironmentObject var settings: Settings
    @EnvironmentObject var keyboard: Keyboard
    @ObservedObject var model: LoginPhoneModel = LoginPhoneModel()

    @State private var showModal = false

    @State private var phoneValid = false


    struct FlagButtonStyle: ButtonStyle {
        func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .foregroundColor(Color.black)
                .padding(.bottom, 45)
                .overlay(
                    Rectangle().frame(height: 1.0)
                        .foregroundColor(configuration.isPressed ? .black : .customGray))
        }
    }



    var body: some View {
        VStack {
            LoginProgressBar(1, back: false)
            Spacer().frame(height: 200)

            Text("RUBL WALLET")
                .font(.custom("Montserrat-Bold", size: 26))
                .foregroundColor(.black)
                .padding([.leading, .trailing])
                .offset(y: -40)

            HStack(spacing: 20) {
                Button(action: {
                    self.showModal.toggle()
                })
                {
                    HStack {
                        Text("\(self.settings.country.flag) \(self.settings.country.phoneCode)")
                            .multilineTextAlignment(.trailing)
                        Image(systemName: "chevron.compact.down")
                    }
                }
                    .buttonStyle(FlagButtonStyle())
                    .sheet(isPresented: $showModal) {
                        LoginCountryView().environmentObject(self.settings)
                }

                TextField("Enter your phone", text: $model.phone)
//                        .onTapGesture {
//                            self.phone = ""
//                        }
                .disableAutocorrection(true)
                    .keyboardType(.numberPad)
//                    .foregroundColor(.white)
                .foregroundColor(.black) //TODO
                .accentColor(.clear)
                    .overlay(
                        HStack {
                            Text(self.model.validatePhoneString(model.phone, countryNameCode: self.settings.country.nameCode))
                            Spacer()
                        })
                    .padding(.bottom, 45)
                    .overlay(
                        PhoneTextFieldOverlay(isWrong: model.isWrongPhoneNumber(model.phone, countryNameCode: self.settings.country.nameCode)))
                    .textContentType(.telephoneNumber)
            }
                .font(.custom("Montserrat-Regular", size: 17))
                .padding(.horizontal, 30.0)

            Text("Changed your phone number?").foregroundColor(.customGray)
                .font(.custom("Montserrat-Regular", size: 13))

            Spacer()

            KeyboardObservingView {
                VStack {
                    if model.isPhoneValid(self.model.phone, countryNameCode: settings.country.nameCode) {

                        Button(action: {
                            self.settings.phone = self.model.getPhone(self.model.phone, countryNameCode: self.settings.country.nameCode)

                            postman.checkPhoneNumber(self.model.phone, completion: {phoneAvailable in
                                print(phoneAvailable)
                                if !phoneAvailable.available {
                                    self.settings.screen = .loginPincode
                                }
                            })
                        }) {
                            HStack {
                                Spacer()
                                Text("Next")
                                Spacer()
                            }
                        }
                            .buttonStyle(LoginNextButtonStyle())

                    } else {
                        Button (action: { }) {
                            HStack {
                                Spacer()
                                Text("Next")
                                Spacer()
                            }
                        }
                            .buttonStyle(LoginNextInactiveButtonStyle())
                    }
                }.padding()
            }
        }
    }
}






struct LoginPhoneScreen_Previews: PreviewProvider {
    static let keyboard = Keyboard()
    static let settings = Settings()
    static var previews: some View {
        Group {
            LoginPhoneScreen().environmentObject(settings).environmentObject(keyboard)
//            .environment(\.colorScheme, .dark)
        }
    }
}

