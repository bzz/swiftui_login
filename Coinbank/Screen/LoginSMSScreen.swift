import SwiftUI


class LoginSMSModel: ObservableObject {
    @Published var pin: [Int] = [-1, -1, -1, -1]
    @Published var pin_repeat: [Int] = [-1, -1, -1, -1]
    @Published var pin_status: PinStatus = PinStatus.pin_enter
}

struct LoginSMSScreen: View {
    @EnvironmentObject var settings: Settings
    @EnvironmentObject var keyboard: Keyboard

    var body: some View {
        VStack {
            LoginProgressBar(3, back: true)

            Text("Enter 6-digit code")
                .foregroundColor(Color.black)
                .font(.custom("Montserrat-Regular", size: 20))
                .padding(13.0)

            Text("We have sent a code to \(self.settings.phone)")
                .foregroundColor(Color.customGray)
                .font(.custom("Montserrat-Regular", size: 13))
                .padding(20)
            Spacer().frame(height: 200)
            Spacer()
            Spacer()


        }
    }
}

struct LoginSMSScreen_Previews: PreviewProvider {
    static let settings = Settings()
    static var previews: some View {
        LoginSMSScreen().environmentObject(settings)
    }
}
