import SwiftUI


struct LoginNextButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(.custom("Montserrat-SemiBold", size: 15))
            .padding(20.0)
            .foregroundColor(.black)
            .background(configuration.isPressed ? Color.customYellow : Color.darkYellow)
            .opacity(configuration.isPressed ? 0.5 : 1.0)
            .cornerRadius(50.0)
    }
}

struct LoginNextInactiveButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(.custom("Montserrat-SemiBold", size: 15))
            .padding(20.0)
            .foregroundColor(.lightGray)
            .background(Color.lightYellow)
            .cornerRadius(50.0)
            .foregroundColor(.white)
            .overlay(
                RoundedRectangle(cornerRadius: 50)
                    .stroke(Color.red, lineWidth: configuration.isPressed ? 1 : 0)
            )
    }
}

struct PhoneTextFieldOverlay: View {
    var isWrong: Bool

    var body: some View {
        VStack {
            if isWrong {
                VStack {
                    Rectangle().frame(height: 2.0)
                    HStack {
                        Text("Wrong phone format")
                            .font(.custom("Montserrat-Regular", size: 12))
                            .foregroundColor(.red)
                        Spacer()
                    }
                        .offset(y: -5)
                }
                    .foregroundColor(.red)
            } else {
                VStack {
                    Rectangle().frame(height: 1.0)
                    HStack {
                        Text(" ")
                            .font(.custom("Montserrat-Regular", size: 12))
                        Spacer()
                    }
                }
                    .foregroundColor(.black)
            }
        }
            .offset(y: 12)
    }
}
