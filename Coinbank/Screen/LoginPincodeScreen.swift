import SwiftUI
import Combine


enum PinStatus {
    case pin_enter
    case pin_repeat
    case pin_repeat_wrong
}

class LoginPincodeModel: ObservableObject {
    @Published var pin: [Int] = [-1, -1, -1, -1]
    @Published var pin_repeat: [Int] = [-1, -1, -1, -1]
    @Published var pin_status: PinStatus = PinStatus.pin_enter
}


struct LoginPincodeScreen: View {
    @EnvironmentObject var settings: Settings
    @ObservedObject var model: LoginPincodeModel = LoginPincodeModel()

    func statusView() -> AnyView {
        var header: String
        var footer: String
        var footerColor = Color.black

        switch model.pin_status {
        case .pin_enter:
            header = "Set up PIN-code"
            footer = "By creating PIN-code you are accepting terms of use and privacy policy"
        case .pin_repeat:
            header = "Repeat PIN-code"
            footer = ""
        case .pin_repeat_wrong:
            header = "Set up PIN-code"
            footer = "PIN-codes don't match"
            footerColor = .red
        }
        return AnyView(VStack {
            Text(header)
                .foregroundColor(Color.black)
                .font(.custom("Montserrat-Regular", size: 20))
                .padding(13.0)
            PincodePad(model: self.model).environmentObject(settings)
            Spacer()
            Text(footer)
                .foregroundColor(footerColor)
                .font(.custom("Montserrat-Regular", size: 12))
                .padding(20.0)
                .fixedSize(horizontal: false, vertical: true)
                .frame(height: 40)
                .multilineTextAlignment(.center)
            Spacer().frame(height: 40)
        })

    }

    var body: some View {
        VStack {
            LoginProgressBar(2, back: true)
            self.statusView().offset(y: 40)
        }
            .foregroundColor(Color.customGray)
            .navigationBarHidden(true)
            .navigationBarTitle(Text(""), displayMode: .inline)
    }

}



struct LoginPincodeScreen_Previews: PreviewProvider {
    static let settings = Settings()
    static var previews: some View {
        LoginPincodeScreen(model: LoginPincodeModel()).environmentObject(settings)
    }
}

