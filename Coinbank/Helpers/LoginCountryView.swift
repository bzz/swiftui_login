import SwiftUI


struct CountryRow: View {
    @EnvironmentObject var settings: Settings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var country: Country
    init(_ country: Country) {
        self.country = country
    }
    var body: some View {
        HStack {
            Button(action: {
                self.settings.country = self.country
                self.presentationMode.wrappedValue.dismiss()
            })
            {
                HStack {
                    Text(country.flag)
                        .font(.largeTitle)
                        .multilineTextAlignment(.leading)

                    Text(country.englishName)
                        .foregroundColor(Color.black)
                    Spacer()
                    Text(country.phoneCode)
                        .foregroundColor(.customGray)
                        .multilineTextAlignment(.trailing)
                }
                    .padding(.horizontal, 20.0)
            }

        }
    }
}

struct LoginCountryView: View {
    @EnvironmentObject var settings: Settings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>


    @State private var searchText = ""
    @State private var showCancelButton: Bool = false


    var body: some View {
        VStack {
            HStack {
                HStack {
                    Image(systemName: "magnifyingglass")

                    TextField("Search", text: $searchText, onEditingChanged: { isEditing in
                        self.showCancelButton = true
                    }, onCommit: {
                            print("onCommit")
                        })
                        .padding()
                        .disableAutocorrection(true)

                    Button(action: {
                        self.searchText = ""
                    }) {
                        Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                    }
                }
                    .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))

                if showCancelButton {
                    Button("Cancel") {
                        UIApplication.shared.endEditing(true)
                        self.searchText = ""
                        self.showCancelButton = false
                    }
                        .foregroundColor(Color(.systemBlue))
                }
            }
                .padding(.horizontal)
                .navigationBarHidden(showCancelButton)
            Divider()

            List {
                ForEach(countries.filter {
                    $0.englishName.hasPrefix(searchText) ||
                        $0.name.hasPrefix(searchText) ||
                        $0.nameCode.hasPrefix(searchText.lowercased()) ||
                        $0.phoneCode.hasPrefix("+" + searchText) ||
                        searchText == "" }, id: \.self) { country in
                    CountryRow(country).environmentObject(self.settings)
                }
            }
                .navigationBarTitle(Text("Search"))
                .resignKeyboardOnDragGesture()
        }
    }
}

struct LoginCountryView_Previews: PreviewProvider {
    static let settings = Settings()
    static var previews: some View {
        LoginCountryView().environmentObject(settings)
    }
}


