import SwiftUI


struct LoginProgressBar: View {
    @EnvironmentObject var settings: Settings
    var progress = 0
    var back: Bool

    init(_ progress: Int, back: Bool) {
        assert(progress >= 0 && progress <= 3)
        self.progress = progress
        self.back = back
    }
    var body: some View {
        ZStack {
            HStack {
                if self.back {
                    Button(action: {
                        self.settings.screen = .loginPhone
                    }) {
                        Image(systemName: "arrow.left")
                    }
                        .frame(width: 40, height: 40)
                    .offset(x: 20, y: 26)
                        .foregroundColor(.black)
                } else {
                    Spacer()
                    .frame(width: 40, height: 40)
                    .offset(y: 26)
                }
                Spacer()
            }
            HStack {
                ForEach(0 ..< progress) { _ in
                    Capsule().fill(Color.customYellow)
                }
                ForEach(progress ..< 3) { _ in
                    Capsule().fill(Color.customGray)
                }
            }.frame(width: 200, height: 4)
                .offset(y: 26)
        }
    }
}
