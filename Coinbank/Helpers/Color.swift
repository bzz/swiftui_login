import SwiftUI


extension Color {
    public static let customGray = Color(hex: 0xB5B9C7)
    public static let lightGray = Color(hex: 0xE4E6ED)
    public static let paleGray = Color(hex: 0xF5F5F5)
    public static let customYellow = Color(hex: 0xFFCE49)
    public static let darkYellow = Color(hex: 0xEBBE42)
    public static let lightYellow = Color(hex: 0xFFF1C9)
    public static let skyYellow = Color(hex: 0xFDF7E4)
    public static let red = Color(hex: 0xFF0000)
    public static let green = Color(hex: 0x19C97E)
}

extension Color {
    init(hex: Int, alpha: Double = 1) {
        let components = (
            R: Double((hex >> 16) & 0xff) / 255,
            G: Double((hex >> 08) & 0xff) / 255,
            B: Double((hex >> 00) & 0xff) / 255
        )
        self.init(.sRGB,
            red: components.R,
            green: components.G,
            blue: components.B,
            opacity: alpha
        )
    }
}
