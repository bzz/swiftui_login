import Foundation


struct Country: Hashable, Codable {
    var name: String = ""
    var englishName: String = ""
    var nameCode: String = ""
    var phoneCode: String = ""
    var flag: String = ""

    init(name: String, englishName: String, nameCode: String, phoneCode: String, flag: String) {
        self.name = name
        self.englishName = englishName
        self.nameCode = nameCode
        self.phoneCode = phoneCode
        self.flag = flag
    }

}
