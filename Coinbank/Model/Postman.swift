import Foundation


open class Postman: NSObject, URLSessionDelegate {

//    var Q: DispatchQueue?
//    override init() {
//        super.init()
//        Q = DispatchQueue(label: "postman.background.q", attributes: []);
//    }



    func checkPhoneNumber(_ number: String, completion: @escaping ((Response.PhoneAvailable) -> ())) {
//        DispatchQueue.global(qos: .default).async {

        let url = URL(string: "https://dev.coinbank.world/v1/auth/checkPhoneNumber")
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        let postString = """
            {
              "phoneNumber": "\(number)"
            }
            """
        request.httpBody = postString.data(using: .utf8)


        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler:
                    { data, response, error in
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        return
                    }
                    do {
                        if let s = String.init(data: data!, encoding: String.Encoding.utf8) {
                            print("GOT DATA FROM SERVER: \(s)")

                            let obj = try
                                JSONDecoder().decode(Response.PhoneAvailable.self, from: data!)
                            DispatchQueue.main.async {
                                completion(obj)
                            }
                        }

                        //NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
//                    let obj = try!
//                        JSONDecoder().decode(PhoneAvailable.self, from: data!)
//                        DispatchQueue.main.async {
//print(obj)
//                    completionHandler(obj)

                        //                       }


                    } catch let error {
                        print(error)
                        let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error could not parse JSON: '\(String(describing: jsonStr))'")
                    }

            })
        task.resume()


    }

}



//               if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                  print(json.description)
//                  DispatchQueue.main.async {
//
//                     model.productsArray.removeAllObjects()
//                     guard let products = json["products"] as? NSArray else { return }
//                     for c in products {
//                        model.productsArray.add(c)
//                     }
//
//                     Notifier.post(.gotStockData)
//                  }
//
//               } else {
//                  let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//                  print("Error could not parse JSON: \(String(describing: jsonStr))")
//               }









