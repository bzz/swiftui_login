import Foundation
import Combine


public enum CurrentScreenStatus {
    case loginPhone
    case loginPincode
    case loginSMS
    case main
}

final class Settings: ObservableObject {
    @Published var screen: CurrentScreenStatus = .loginPhone
//    typealias PublisherType = PassthroughSubject<Void, Never>

//    var willChange = PassthroughSubject<Void, Never>()
//    var didChange = PassthroughSubject<Void, Never>()

    @Published var country = Country(name: "Российская Федерация", englishName: "Russian Federation", nameCode: "ru", phoneCode: "+7", flag: "🇷🇺")
    @Published var pin: [Int] = UserDefaults.standard.array(forKey: "pin") as? [Int] ?? [Int]()
    @Published var phone: String = UserDefaults.standard.string(forKey: "phone") ?? ""

    func save() {
        UserDefaults.standard.set(pin, forKey: "pin")
        UserDefaults.standard.set(phone, forKey: "phone")
    }


}
