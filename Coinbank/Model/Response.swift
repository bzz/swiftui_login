import Foundation

enum Response {
    struct PhoneAvailable: Codable {
        var available: Bool
    }
}
